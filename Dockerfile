FROM python:3-alpine3.17

RUN apk add --no-cache "build-base" "musl-dev" "libffi-dev" "boost-dev>1.76.0" "boost-build>1.76.0" "boost-static>1.76.0" "openssl-dev>1.1.0" "openssl-libs-static>1.1.0" "zlib-dev>1.2.0" "zlib-static>1.2.0" "rust>1.41" "cargo" "libmagic" "file-dev" "imagemagick" "imagemagick-libs" "imagemagick-dev"
RUN pip install --upgrade pip
RUN pip install poetry
ADD ./poetry.lock /base/poetry.lock
ADD ./pyproject.toml /base/pyproject.toml
RUN cd base; poetry config virtualenvs.create false; poetry install
